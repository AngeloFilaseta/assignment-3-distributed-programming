package actors.cluster.messages.initBoard;

import actors.cluster.messages.PuzzleMessages;
import actors.cluster.util.LamportClock;

public final class InitializeBoardRequest extends PuzzleMessages {

    private InitializeBoardRequest(final LamportClock clock) {
        super(clock);
    }

    public static InitializeBoardRequest createInstance(final LamportClock clock) {
        return new InitializeBoardRequest(clock);
    }
}
