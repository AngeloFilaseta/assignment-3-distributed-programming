/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Java application project to get you started.
 * For more details take a look at the 'Building Java & JVM projects' chapter in the Gradle
 * User Manual available at https://docs.gradle.org/6.8.3/userguide/building_java_projects.html
 */

plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    mavenCentral()
}

val akkaVersion = "2.6.14";
val scalaBinary = "2.13";
val akkaManagementVersion = "1.1.0"

dependencies {
    // Use JUnit test framework.
    testImplementation("junit:junit:4.13")

    // This dependency is used by the application.
    implementation("de.ruedigermoeller:fst:2.49")
    implementation("com.google.guava:guava:29.0-jre")
    implementation("com.typesafe.akka:akka-cluster_${scalaBinary}:${akkaVersion}")
    implementation("com.typesafe.akka:akka-stream_${scalaBinary}:${akkaVersion}")
    //implementation("com.typesafe.akka:akka-bom_${scalaBinary}:${akkaVersion}")
    implementation(platform("com.typesafe.akka:akka-bom_${scalaBinary}:${akkaVersion}"))
    implementation("com.typesafe.akka:akka-serialization-jackson_${scalaBinary}:${akkaVersion}")
    implementation("com.typesafe.akka:akka-distributed-data_${scalaBinary}:${akkaVersion}")

    implementation("org.slf4j:slf4j-api:1.7.5")
    implementation("org.slf4j:slf4j-log4j12:1.7.5")
}

task("runActors", JavaExec::class) {
    main = "actors.App"
    classpath = sourceSets["main"].runtimeClasspath
}

task("runRmi", JavaExec::class) {
    main = "rmi.main.Node"
    classpath = sourceSets["main"].runtimeClasspath
}
