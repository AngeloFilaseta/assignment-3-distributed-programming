package actors.cluster.handlers;

import actors.cluster.ModelSingleton;
import actors.cluster.messages.updateBoard.UpdateBoardAck;
import actors.cluster.messages.updateBoard.UpdateBoardRequest;
import actors.cluster.messages.updateBoard.UserSwapAction;
import actors.cluster.puzzleCoordination.PuzzleCoordinationSingleton;
import actors.cluster.util.ClusterUtil;
import actors.cluster.util.CriticalSectionOperationsEnum;
import actors.puzzleboard.PuzzleBoard;

public final class UpdateMessagesHandlers {

    private UpdateMessagesHandlers() { }

    public static void onUserSwapAction(final UserSwapAction msg) {
        PuzzleBoard puzzleBoard = ModelSingleton.getInstance().getBoard();
        new Thread(() -> puzzleBoard.getSelectionManager().performSwap(() ->
            puzzleBoard.paintPuzzle(puzzleBoard.getPanel())
        )).start();
        PuzzleCoordinationSingleton.getInstance().requestOperation(CriticalSectionOperationsEnum.UPDATE);
    }

    public static void onUpdateBoardRequest(final UpdateBoardRequest msg) {
        PuzzleCoordinationSingleton
                .getInstance()
                .handleUpdateRequestReceived(
                        ClusterUtil.getSenderAddress(),
                        msg.getTiles(),
                        msg.getClock()
                );
    }

    public static void onUpdateBoardAck(final UpdateBoardAck msg) {
        PuzzleCoordinationSingleton
                .getInstance()
                .handleUpdateAckReceived(
                        ClusterUtil.getSenderAddress(),
                        msg.getClock()
                );
    }
}
