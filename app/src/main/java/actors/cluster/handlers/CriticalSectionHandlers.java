package actors.cluster.handlers;

import actors.cluster.messages.criticalSection.AskLock;
import actors.cluster.messages.criticalSection.GrantLock;
import actors.cluster.puzzleCoordination.PuzzleCoordinationSingleton;
import actors.cluster.util.ClusterUtil;

public final class CriticalSectionHandlers {

    private CriticalSectionHandlers() { }

    public static void onAskLock(final AskLock event) {
        PuzzleCoordinationSingleton
                .getInstance()
                .handleLockRequestReceived(
                        ClusterUtil.getSenderAddress(),
                        event.getClock()
                );
    }

    public static void onGrantLock(final GrantLock event) {
        PuzzleCoordinationSingleton
                .getInstance()
                .handleLockAckReceived(ClusterUtil.getSenderAddress(), event.getClock());
    }

}
