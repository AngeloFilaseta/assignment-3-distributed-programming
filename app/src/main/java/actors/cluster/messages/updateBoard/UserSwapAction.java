package actors.cluster.messages.updateBoard;

import java.io.Serializable;

public final class UserSwapAction implements Serializable {

    private UserSwapAction() { }

    public static UserSwapAction createInstance() {
        return new UserSwapAction();
    }
}
