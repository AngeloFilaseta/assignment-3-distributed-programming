package actors.cluster;

import actors.puzzleboard.PuzzleBoard;
import actors.puzzleboard.Tile;
import akka.actor.ActorRef;

import java.util.List;

public final class ModelSingleton {

    private static ModelSingleton instance;

    private PuzzleBoard board;
    private final ActorRef actorRef;

    public static void createInstance(final ActorRef actorRef) {
        if (instance == null) {
            instance = new ModelSingleton(actorRef);
        } else {
            throw new IllegalCallerException("Cannot re-create this singleton instance");
        }
    }

    public static ModelSingleton getInstance() {
        if (instance == null) {
            throw new IllegalCallerException("Singleton not created.");
        }
        return instance;
    }

    public static void updateInstance(final ActorRef actorRef, final List<Tile> tiles) {
        if (instance == null) {
            instance = new ModelSingleton(actorRef, tiles);
        } else {
            instance.getBoard().setListOfTiles(tiles);
            instance.getBoard().paintPuzzle(instance.getBoard().getPanel());
        }
    }

    private ModelSingleton(final ActorRef actorRef) {
        this.actorRef = actorRef;
        createPuzzleBoard();
        board.loadImage();
        board.createTiles();
        board.paintPuzzle(board.getPanel());
    }

    private ModelSingleton(final ActorRef actorRef, final List<Tile> tiles) {
        this.actorRef = actorRef;
        createPuzzleBoard();
        board.setListOfTiles(tiles);
        board.paintPuzzle(board.getPanel());
    }

    public PuzzleBoard getBoard() {
        return board;
    }

    private void createPuzzleBoard() {
        board = new PuzzleBoard(actorRef, "Actors Puzzle");
        board.setVisible(true);
    }

}
