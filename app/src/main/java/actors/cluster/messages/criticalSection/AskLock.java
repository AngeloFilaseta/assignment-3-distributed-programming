package actors.cluster.messages.criticalSection;

import actors.cluster.messages.PuzzleMessages;
import actors.cluster.util.LamportClock;

public final class AskLock extends PuzzleMessages {

    private AskLock(final LamportClock clock) {
        super(clock);
    }

    public static AskLock createInstance(final LamportClock clock) {
        return new AskLock(clock);
    }
}
