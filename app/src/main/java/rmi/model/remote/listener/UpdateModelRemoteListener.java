package rmi.model.remote.listener;

import rmi.model.local.singleton.LocalModelSingleton;
import rmi.model.puzzleboard.Tile;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public final class UpdateModelRemoteListener implements RemoteListener {

    public static UpdateModelRemoteListener createListener() {
        return new UpdateModelRemoteListener();
    }

    @Override
    public void onStateChange(final List<Tile> newTileList) throws RemoteException {
        LocalModelSingleton.getInstance().getBoard().setListOfTiles(newTileList);
    }

    @Override
    public void onListenerAdd(final Set<RemoteListener> remoteListeners) throws RemoteException {
        LocalModelSingleton.getInstance().setRemoteListenersReferences(remoteListeners);
    }

}
