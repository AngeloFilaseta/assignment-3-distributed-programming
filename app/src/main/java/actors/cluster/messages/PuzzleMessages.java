package actors.cluster.messages;

import actors.cluster.util.LamportClock;

import java.io.Serializable;

public abstract class PuzzleMessages implements Serializable {

    private final LamportClock clock;

    public PuzzleMessages(final LamportClock clock) {
        this.clock = clock;
    }

    /**
     * @return the LamportClock of the message.
     */
    public LamportClock getClock() {
        return clock;
    }

}
