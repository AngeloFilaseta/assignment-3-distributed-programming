package rmi.model.remote.name;

public enum RemoteName {

    MODEL;

    @Override
    public String toString() {
        if (this == RemoteName.MODEL) {
            return "MODEL";
        }
        throw new IllegalCallerException("No name found for this Enum.");
    }
}
