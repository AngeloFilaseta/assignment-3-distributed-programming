package rmi.model.local;

import rmi.model.puzzleboard.PuzzleBoard;
import rmi.model.remote.listener.RemoteListener;

import java.util.Set;

public interface LocalModel {

    PuzzleBoard getBoard();

    Set<RemoteListener> getRemoteListenersReferences();

    void setRemoteListenersReferences(Set<RemoteListener> updatedRemoteListeners);

    void setBoardVisible();
}
