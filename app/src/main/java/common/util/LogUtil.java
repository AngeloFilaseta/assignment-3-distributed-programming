package common.util;

public final class LogUtil {

    private LogUtil() {

    }

    public static void log(final String subject, final String content) {
        System.out.println("[" + subject + "]: " + content);
    }
}
