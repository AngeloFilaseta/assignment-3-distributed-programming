package actors.cluster.messages.criticalSection;

import actors.cluster.messages.PuzzleMessages;
import actors.cluster.util.LamportClock;

public final class GrantLock extends PuzzleMessages {

    private GrantLock(final LamportClock clock) {
        super(clock);
    }

    public static GrantLock createInstance(final LamportClock clock) {
        return new GrantLock(clock);
    }
}
