package actors.cluster.puzzleCoordination;

import actors.cluster.util.ClusterUtil;
import actors.cluster.util.LamportClock;

import static actors.cluster.util.LamportClock.LamportClockCompareResult.LESS;

public final class MyCurrentLamportClockSingleton {

    private static MyCurrentLamportClockSingleton instance = null;
    private LamportClock lamportClock;

    private MyCurrentLamportClockSingleton() { }

    public static MyCurrentLamportClockSingleton getInstance() {
        if (instance == null) {
            instance = new MyCurrentLamportClockSingleton();
        }
        return instance;
    }

    public LamportClock getLamportClock() {
        return lamportClock;
    }

    public void setLamportClock(final LamportClock lamportClock) {
        this.lamportClock = lamportClock;
    }

    public void incrementMyLamportClock() {
        setLamportClock(lamportClock.increment(ClusterUtil.getMyAddress().toString()));
    }

    public void updateMyCurrentClockIfNeeded(final LamportClock msgClock) {
        if (lamportClock.compareWith(msgClock).equals(LESS)) {
            lamportClock = msgClock;
        }
    }


}
