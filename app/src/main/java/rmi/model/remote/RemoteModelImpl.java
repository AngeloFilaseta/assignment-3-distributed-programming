package rmi.model.remote;

import rmi.model.puzzleboard.Tile;
import rmi.model.remote.listener.RemoteListener;

import java.rmi.RemoteException;
import java.util.*;

public final class RemoteModelImpl implements RemoteModel {

    private List<Tile> tileList;
    private final Set<RemoteListener> listeners;

    public static RemoteModelImpl createRemoteTileContainer(final List<Tile> tileList) {
        return new RemoteModelImpl(tileList);
    }

    private RemoteModelImpl(final List<Tile> tileList) {
        this.tileList = tileList;
        this.listeners = new HashSet<>();
    }

    @Override
    public List<Tile> getTileList() throws RemoteException {
        return tileList;
    }

    @Override
    public synchronized void updateTileList(final List<Tile> tileList) throws RemoteException {
        this.tileList = tileList;
        broadcastStateChange();
    }

    @Override
    public synchronized void addListener(final RemoteListener listener) throws RemoteException {
        listeners.add(listener);
        broadcastListenerChange();
    }

    @Override
    public synchronized void setListeners(final Set<RemoteListener> newListeners) throws RemoteException {
        this.listeners.clear();
        this.listeners.addAll(newListeners);
    }

    private void broadcastStateChange() {
        List<RemoteListener> listenersToRemove = new ArrayList<>();
        for (RemoteListener listener : listeners) {
            try {
                listener.onStateChange(tileList);
            } catch (RemoteException e) {
                listenersToRemove.add(listener);
            }
        }
        listenersToRemove.forEach(listeners::remove);
    }


    private void broadcastListenerChange() {
        List<RemoteListener> listenersToRemove = new ArrayList<>();
        for (RemoteListener listener : listeners) {
            try {
                listener.onListenerAdd(listeners);
            } catch (RemoteException e) {
                listenersToRemove.add(listener);
            }
        }
        listenersToRemove.forEach(listeners::remove);
    }
}
