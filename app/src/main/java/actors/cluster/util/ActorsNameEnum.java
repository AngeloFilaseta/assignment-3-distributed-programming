package actors.cluster.util;

public enum ActorsNameEnum {

    /**
     * ActorSystem name.
     */
    SYSTEM,
    /**
     * Main cluster name.
     */
    CLUSTER;

    @Override
    public String toString() {
        if (this == ActorsNameEnum.SYSTEM) {
            return "ClusterSystem";
        } else if (this == ActorsNameEnum.CLUSTER) {
            return "ClusterListener";
        }
        throw new IllegalCallerException();
    }

}
