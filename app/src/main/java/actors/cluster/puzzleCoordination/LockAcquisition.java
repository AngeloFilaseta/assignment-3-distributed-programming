package actors.cluster.puzzleCoordination;

import actors.cluster.ModelSingleton;
import actors.cluster.messages.criticalSection.AskLock;
import actors.cluster.messages.criticalSection.GrantLock;
import actors.cluster.messages.initBoard.InitializeBoardRequest;
import actors.cluster.messages.updateBoard.UpdateBoardRequest;
import actors.cluster.util.AloneOperations;
import actors.cluster.util.ClusterUtil;
import actors.cluster.util.CriticalSectionOperationsEnum;
import actors.cluster.util.LamportClock;
import akka.actor.Address;

import java.util.HashSet;
import java.util.Set;

import static actors.cluster.util.CriticalSectionOperationsEnum.*;
import static actors.cluster.util.LamportClock.LamportClockCompareResult.*;

public final class LockAcquisition {

    private final MyCurrentLamportClockSingleton myCurrentLamportClock;
    private final EnqueuedLockRequests enqueuedLockRequests;
    private final Peers peers;

    private CriticalSectionOperationsEnum operationRequested;
    private LamportClock myLockRequestClock;
    private boolean isInterestedInLock;
    private boolean isLockAcquired;
    private final Set<Address> ackReceived;

    private LockAcquisition(final EnqueuedLockRequests enqueuedLockRequests, final Peers peers) {
        this.myCurrentLamportClock = MyCurrentLamportClockSingleton.getInstance();
        this.enqueuedLockRequests = enqueuedLockRequests;
        this.peers = peers;
        isInterestedInLock = false;
        isLockAcquired = false;
        operationRequested = NONE;
        ackReceived = new HashSet<>();
    }

    public static LockAcquisition createInstance(final EnqueuedLockRequests enqueuedLockRequests, final Peers peers) {
        return new LockAcquisition(enqueuedLockRequests, peers);
    }

    public boolean isLockAcquired() {
        return isLockAcquired;
    }

    public void startLockAcquisition(final CriticalSectionOperationsEnum operation) {
        if (peers.isLastPeerInCluster() && operation.equals(INITIALIZE)) {
            //no other useful peer
            AloneOperations.initRandomBoard();
        } else if (!peers.isLastPeerInCluster()) {
            System.out.println("Start lock acquisition for " + operation + ", broadcasting lock request to initialized peers");
            isInterestedInLock = true;
            //saving lamport clock at request time
            myLockRequestClock = myCurrentLamportClock.getLamportClock().increment(ClusterUtil.getMyAddress().toString());
            myCurrentLamportClock.setLamportClock(myLockRequestClock);
            operationRequested = operation;
            //broadcasting AskLock
            ClusterUtil.broadcastTo(peers.getAllPeers(), AskLock.createInstance(myLockRequestClock));
        }
    }

    public void releaseLock() {
        System.out.println("Releasing lock and sending ack to enqueued lock requests");
        isLockAcquired = false;
        ackReceived.clear();
        isInterestedInLock = false;
        operationRequested = NONE;
        myCurrentLamportClock.incrementMyLamportClock();
        //broadcasting eventual enqueued lock request (also clearing enqueued lock request list)
        ClusterUtil.broadcastTo(
                enqueuedLockRequests.clearLockRequests(),
                GrantLock.createInstance(myCurrentLamportClock.getLamportClock())
        );
    }

    public void handleLockAckReceived(final Address address, final LamportClock msgClock) {
        myCurrentLamportClock.updateMyCurrentClockIfNeeded(msgClock);
        myCurrentLamportClock.incrementMyLamportClock();
        if (!isLockAcquired && isInterestedInLock) {
            System.out.println("Lock ack received from " + address.toString());
            ackReceived.add(address);
            if (areAllAckReceived()) {
                onAllAckReceived();
            }
        }
    }

    public void handleLockRequestReceived(final Address address, final LamportClock reqClock) {
        myCurrentLamportClock.updateMyCurrentClockIfNeeded(reqClock);
        myCurrentLamportClock.incrementMyLamportClock();
        String log = "Lock request received from " + address.toString() + ", ";
        if (isLockAcquired || (isInterestedInLock && myLockRequestClock.compareWith(reqClock).equals(LESS))) {
            //already in critical section or having a lock request that came PRIOR compared to received lock request
            enqueuedLockRequests.addLockRequest(address);
            log += "moving to enqueued lock requests";
        } else {
            //granting lock
            ClusterUtil.sendMessageTo(address, GrantLock.createInstance(myCurrentLamportClock.getLamportClock()));
            log += "sending ack";
        }
        System.out.println(log);
    }

    public void handlePeerLeaving(final Address address) {
        //not afflicted unless interested in lock but NOT already obtained
        if (isInterestedInLock && !isLockAcquired) {
            ackReceived.remove(address);
            if (peers.isLastPeerInCluster() || peers.isLastInitializedPeerInCluster()) {
                if (operationRequested.equals(INITIALIZE)) {
                    AloneOperations.initRandomBoard();
                }
                releaseLock();
            } else if (areAllAckReceived()) {
                onAllAckReceived();
            }
        }
    }

    public void handlePeerJoining(final Address address) {
        //not afflicted unless interested in lock but NOT already obtained
        if (isInterestedInLock && !isLockAcquired) {
            System.out.println("Sending lock request to " + address.toString());
            ClusterUtil.sendMessageTo(address, AskLock.createInstance(myLockRequestClock));
        }
    }

    private boolean areAllAckReceived() {
        return ackReceived.size() == peers.getAllPeers().size();
    }

    private void onAllAckReceived() {
        System.out.println("All lock ack received, entering critical section to perform " + operationRequested);
        myCurrentLamportClock.incrementMyLamportClock();
        isLockAcquired = true;
        ackReceived.clear();
        switch (operationRequested) {
            case INITIALIZE:
                ClusterUtil.broadcastTo(
                    peers.getInitializedPeers(),
                    InitializeBoardRequest.createInstance(myCurrentLamportClock.getLamportClock()));
            case UPDATE:
                ClusterUtil.broadcastTo(
                    peers.getInitializedPeers(),
                    UpdateBoardRequest.createInstance(
                            myCurrentLamportClock.getLamportClock(),
                            ModelSingleton.getInstance().getBoard().getListOfTiles()
                    )
                );
            default:
                throw new IllegalArgumentException("Invalid message type.");
        }
    }

}
