package actors.puzzleboard;

public final class SelectionManager {

    private boolean selectionActive = false;
    private Tile firstTile;
    private Tile secondTile;

    public void selectTile(final Tile tile, final LockLister listener) {
        if (selectionActive) {
            secondTile = tile;
            listener.onSecondTileSelection();
        } else {
            selectionActive = true;
            firstTile = tile;
        }
    }

    public void performSwap(final Listener listener) {
        selectionActive = false;
        swap(firstTile, secondTile);
        listener.onSwapPerformed();
    }

    private void swap(final Tile t1, final Tile t2) {
        int pos = t1.getCurrentPosition();
        t1.setCurrentPosition(t2.getCurrentPosition());
        t2.setCurrentPosition(pos);
    }

    @FunctionalInterface
    public interface Listener {
        void onSwapPerformed();
    }

    interface LockLister {
        void onSecondTileSelection();
    }
}
