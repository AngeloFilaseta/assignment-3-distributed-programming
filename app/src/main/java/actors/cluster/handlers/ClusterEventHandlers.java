package actors.cluster.handlers;

import actors.cluster.util.ClusterReferencesSingleton;
import actors.cluster.puzzleCoordination.PuzzleCoordinationSingleton;
import akka.cluster.ClusterEvent;

public final class ClusterEventHandlers {

    private ClusterEventHandlers() { }

    public static void onUnreachableMember(final ClusterEvent.UnreachableMember event) {
        System.out.println("Peer " + event.member().address().toString() + " unreachable, removing it from cluster");
        //removing unreachable member from cluster
        ClusterReferencesSingleton.getInstance().getCluster().down(event.member().address());
    }

    public static void onMemberRemoved(final ClusterEvent.MemberRemoved event) {
        PuzzleCoordinationSingleton.getInstance().handlePeerLeaving(event.member().address());
    }

    public static void onMemberUp(final ClusterEvent.MemberUp event) {
        PuzzleCoordinationSingleton.getInstance().handlePeerJoining(event.member().address());
    }

}
