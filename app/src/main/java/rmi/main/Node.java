package rmi.main;
import common.util.LogUtil;
import rmi.model.local.singleton.LocalModelSingleton;
import rmi.model.remote.RemoteModel;
import rmi.model.remote.RemoteModelImpl;
import rmi.model.remote.name.RemoteName;
import rmi.model.remote.singleton.RemoteModelSingleton;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

public final class Node {

    private static final int DELTA_WAIT_RECONNECT = 500;

    private Node() {
    }

    public static void main(final String[] args) {
        Registry registry;
        try {
            registry = LocateRegistry.getRegistry();
            if (Arrays.asList(registry.list()).contains(RemoteName.MODEL.toString())) {
                connectingNodeBehaviour(registry);
            } else {
                initiatorNodeBehaviour();
            }
        } catch (Exception e) {
            System.err.println("Found Exception: " + e);
            e.printStackTrace();
        }
    }

    /**
     * No RemoteModel Object found. In this case I'm the node that is going to create and register it!
     * @throws RemoteException The classic RMI Exception to handle
     */
    private static void initiatorNodeBehaviour() throws RemoteException {
        LogUtil.log("INITIATOR", "Starting up...");
        LocalModelSingleton.createInstance();
        //Send the list of tiles created to the remote object
        RemoteModel remoteModel = RemoteModelImpl.createRemoteTileContainer(LocalModelSingleton.getInstance().getBoard().getListOfTiles());
        RemoteModelSingleton.createInstance(remoteModel);
        LocalModelSingleton.getInstance().setBoardVisible();
    }

    /**
     *  RemoteModel Object is on the registry. I can retrieve it.
     * @param registry The RMI Registry used to retrieve the Remote Object.
     * @throws RemoteException The classic RMI Exception to handle.
     * @throws NotBoundException Can be thrown by the lookup method of the registry.
     * @throws InterruptedException Thrown by the lookup operation of the RMI Registry. See tryToConnect method.
     */
    private static void connectingNodeBehaviour(final Registry registry) throws RemoteException, NotBoundException, InterruptedException {
        LogUtil.log("CONNECTING", "I'm a connecting Node!");
        LocalModelSingleton.createInstance();
        tryToConnect(registry);
        LocalModelSingleton.getInstance().getBoard().setListOfTiles(RemoteModelSingleton.getInstance().getTileList());
        LocalModelSingleton.getInstance().setBoardVisible();
    }

    /**
     * The RemoteModel Object could exist but it may not be referenced by a initiator node if it crashed or
     * if it left. This method waits until the Remote model is reference again.
     *
     * @param registry The RMI Registry instance.
     * @throws InterruptedException Thrown by the sleep method, used to make polling.
     * @throws NotBoundException Thrown by the lookup operation of the RMI Registry
     */
    private static void tryToConnect(final Registry registry) throws InterruptedException, NotBoundException {
        boolean remoteModelExists = false;
        while (!remoteModelExists) {
            try {
                 RemoteModel remoteModel = (RemoteModel) registry.lookup(RemoteName.MODEL.toString());
                 RemoteModelSingleton.createInstance(remoteModel);
                 remoteModelExists = true;
            } catch (RemoteException e) {
                LogUtil.log("CONNECTING", "Connection failed... Trying again in " + DELTA_WAIT_RECONNECT + "ms");
                Thread.sleep(DELTA_WAIT_RECONNECT);
            }
        }
    }

}
