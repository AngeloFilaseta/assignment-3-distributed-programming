package actors.puzzleboard;

import actors.cluster.messages.updateBoard.UserSwapAction;
import actors.cluster.util.ClusterReferencesSingleton;
import actors.cluster.util.ClusterUtil;
import akka.actor.ActorRef;
import common.util.ResourceUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public final class PuzzleBoard extends JFrame {

    public static final int ROW_SIZE = 3;
    public static final int COL_SIZE = 5;

    private final JPanel board;
    private final int rows;
    private final int columns;
    private List<Tile> tiles = new ArrayList<>();
    private BufferedImage image;

    private final SelectionManager selectionManager = new SelectionManager();

    private final ActorRef actorRef;

    public PuzzleBoard(final ActorRef actorRef, final String title) {
        this.actorRef = actorRef;
        this.rows = ROW_SIZE;
        this.columns = COL_SIZE;
        setTitle(title);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board = new JPanel();
        board.setBorder(BorderFactory.createLineBorder(Color.gray));
        board.setLayout(new GridLayout(rows, columns, 0, 0));
        getContentPane().add(board, BorderLayout.CENTER);
    }

    public void loadImage() {
        try {
            image = ImageIO.read(new File(ResourceUtil.getImageFolderName() + ResourceUtil.getImageName()));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Could not load image", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void createTiles() {
        final int imageWidth = image.getWidth(null);
        final int imageHeight = image.getHeight(null);
        int position = 0;
        final List<Integer> randomPositions = new ArrayList<>();
        IntStream.range(0, rows * columns).forEach(randomPositions::add);
        Collections.shuffle(randomPositions);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                BufferedImage imagePortion = image.getSubimage(j * imageWidth / columns,
                        i * imageHeight / rows,
                        (imageWidth / columns),
                        imageHeight / rows);

                tiles.add(Tile.createTile(createSerializableImage(imagePortion), position, randomPositions.get(position)));
                position++;
            }
        }
    }

    //convert bufferedImage in a byte[] so it can be serialized (I think)
    private byte[] createSerializableImage(final BufferedImage bufferedImage) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] imageInByte = null;
        try {
            ImageIO.write(bufferedImage, "jpg", bos);
            bos.flush();
            imageInByte = bos.toByteArray();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageInByte;
    }

    public void paintPuzzle(final JPanel board) {
        board.removeAll();
        Collections.sort(tiles);
        tiles.forEach(tile -> {
            final TileButton btn = new TileButton(tile);
            board.add(btn);
            btn.setBorder(BorderFactory.createLineBorder(Color.gray));
            btn.addActionListener(actionListener -> selectionManager.selectTile(tile,
                    () -> actorRef.tell(UserSwapAction.createInstance(), actorRef)));
        });
        checkSolution();
        pack();
    }

    public List<Tile> getListOfTiles() {
        return tiles;
    }

    public void setListOfTiles(final List<Tile> updatedTiles) {
        this.tiles = updatedTiles;
    }

    public JPanel getPanel() {
        return board;
    }

    public SelectionManager getSelectionManager() {
        return selectionManager;
    }

    private void checkSolution() {
        if (tiles.stream().allMatch(Tile::isInRightPlace)) {
            ClusterReferencesSingleton
                    .getInstance()
                    .getCluster()
                    .down(ClusterUtil.getMyAddress());
            JOptionPane.showMessageDialog(this, "Puzzle Completed!", "", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }
}
