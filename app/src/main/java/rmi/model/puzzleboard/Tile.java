package rmi.model.puzzleboard;

import java.io.Serializable;

public final class Tile implements Comparable<Tile>, Serializable {

    private final byte[] image;
    private final int originalPosition;
    private int currentPosition;

    public static Tile createTile(final byte[] image, final int originalPosition, final int currentPosition) {
        return new Tile(image, originalPosition, currentPosition);
    }

    private Tile(final byte[] image, final int originalPosition, final int currentPosition) {
        this.image = image;
        this.originalPosition = originalPosition;
        this.currentPosition = currentPosition;
    }

    public byte[] getImage() {
        return image;
    }

    public boolean isInRightPlace() {
        return currentPosition == originalPosition;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(final int newPosition) {
        currentPosition = newPosition;
    }

    @Override
    public int compareTo(final Tile other) {
        return Integer.compare(this.currentPosition, other.currentPosition);
    }

}
