package actors.cluster.handlers;

import actors.cluster.messages.initBoard.InitializeBoardAck;
import actors.cluster.messages.initBoard.InitializeBoardRequest;
import actors.cluster.puzzleCoordination.PuzzleCoordinationSingleton;
import actors.cluster.util.ClusterUtil;

public final class InitializeMessagesHandlers {

    private InitializeMessagesHandlers() { }

    public static void onInitializeBoardRequest(final InitializeBoardRequest msg) {
        PuzzleCoordinationSingleton.getInstance()
                .handleInitRequestReceived(ClusterUtil.getSenderAddress(), msg.getClock());
    }

    public static void onInitializeBoardAck(final InitializeBoardAck msg) {
        PuzzleCoordinationSingleton.getInstance()
                .handleInitAckReceived(ClusterUtil.getSenderAddress(), msg.getTiles(), msg.getClock());
    }

}
