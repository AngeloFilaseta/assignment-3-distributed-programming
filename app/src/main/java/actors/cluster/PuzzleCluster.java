package actors.cluster;

import actors.cluster.handlers.ClusterEventHandlers;
import actors.cluster.handlers.CriticalSectionHandlers;
import actors.cluster.handlers.InitializeMessagesHandlers;
import actors.cluster.handlers.UpdateMessagesHandlers;
import actors.cluster.messages.criticalSection.AskLock;
import actors.cluster.messages.criticalSection.GrantLock;
import actors.cluster.messages.initBoard.InitializeBoardAck;
import actors.cluster.messages.initBoard.InitializeBoardRequest;
import actors.cluster.messages.updateBoard.UpdateBoardAck;
import actors.cluster.messages.updateBoard.UpdateBoardRequest;
import actors.cluster.messages.updateBoard.UserSwapAction;
import actors.cluster.util.ClusterReferencesSingleton;
import akka.actor.AbstractActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;

/**
 * Abstract Actor of the system containing mapping for every message and relative handler.
 */
public final class PuzzleCluster extends AbstractActor {

  private final Cluster cluster = Cluster.get(getContext().getSystem());

  @Override
  public void preStart() {
    ClusterReferencesSingleton.createInstance(cluster, getContext(), getSelf());
    //listening for akka events
    cluster.subscribe(getSelf(),
                      ClusterEvent.MemberUp.class,
                      ClusterEvent.MemberRemoved.class,
                      ClusterEvent.UnreachableMember.class);
    ClusterEvent.initialStateAsEvents();
  }

  @Override
  public void postStop() {
    cluster.unsubscribe(getSelf());
  }

  @Override
  public Receive createReceive() {
    //mapping messages to handlers
    return receiveBuilder()
            .match(ClusterEvent.MemberUp.class, ClusterEventHandlers::onMemberUp)
            .match(ClusterEvent.MemberRemoved.class, ClusterEventHandlers::onMemberRemoved)
            .match(ClusterEvent.UnreachableMember.class, ClusterEventHandlers::onUnreachableMember)
            .match(AskLock.class, CriticalSectionHandlers::onAskLock)
            .match(GrantLock.class, CriticalSectionHandlers::onGrantLock)
            .match(InitializeBoardRequest.class, InitializeMessagesHandlers::onInitializeBoardRequest)
            .match(InitializeBoardAck.class, InitializeMessagesHandlers::onInitializeBoardAck)
            .match(UserSwapAction.class, UpdateMessagesHandlers::onUserSwapAction)
            .match(UpdateBoardRequest.class, UpdateMessagesHandlers::onUpdateBoardRequest)
            .match(UpdateBoardAck.class, UpdateMessagesHandlers::onUpdateBoardAck)
            .build();
  }

}
