package actors.cluster.messages.updateBoard;

import actors.cluster.messages.AbstractBoardMessage;
import actors.cluster.util.LamportClock;
import actors.puzzleboard.Tile;

import java.util.List;

public final class UpdateBoardRequest extends AbstractBoardMessage {

    private UpdateBoardRequest(final LamportClock clock, final List<Tile> tiles) {
        super(clock, tiles);
    }

    public static UpdateBoardRequest createInstance(final LamportClock clock, final List<Tile> tiles) {
        return new UpdateBoardRequest(clock, tiles);
    }
}
