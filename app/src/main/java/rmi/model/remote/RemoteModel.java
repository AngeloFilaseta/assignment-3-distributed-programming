package rmi.model.remote;

import rmi.model.puzzleboard.Tile;
import rmi.model.remote.listener.RemoteListener;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public interface RemoteModel extends Remote {

    List<Tile> getTileList() throws RemoteException;

    void updateTileList(List<Tile> tile) throws RemoteException;

    void addListener(RemoteListener listener) throws RemoteException;

    void setListeners(Set<RemoteListener> listener) throws RemoteException;

}
