package actors.cluster.util;

import actors.cluster.ModelSingleton;
import actors.puzzleboard.PuzzleBoard;

public final class AloneOperations {

    private AloneOperations() { }

    public static void initRandomBoard() {
        System.out.println("No initialized peers in cluster, executing init random board");
        ModelSingleton.createInstance(ClusterReferencesSingleton.getInstance().getSelf());
    }

    public static void updateBoard() {
        System.out.println("No initialized peers in cluster, executing update board");
        PuzzleBoard puzzleBoard = ModelSingleton.getInstance().getBoard();
        new Thread(() -> puzzleBoard.getSelectionManager().performSwap(() -> {
            puzzleBoard.paintPuzzle(puzzleBoard.getPanel());
        })).start();
    }
}
