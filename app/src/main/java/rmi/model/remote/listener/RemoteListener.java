package rmi.model.remote.listener;

import rmi.model.puzzleboard.Tile;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * The Listener class to notify nodes about the remote object state change.
 * Listeners are also stored in each node to restore the situation if the initiator node goes down.
 * IMPORTANT:
 * Notice how this interface extends the Remote Interface.
 * This is needed as the listener will not get serialized when sent as a message,
 * it will be passed by reference allowing method calls from remote.
 */
public interface RemoteListener extends Remote {

    /**
     * Do something when the remote state is updated.
     * @param newTileList the updated new tile list.
     * @throws RemoteException The classic RMI exception to handle.
     */
    void onStateChange(List<Tile> newTileList) throws RemoteException;

    /**
     * Do something when a listener is added to the remote object.
     * @param remoteListeners the updated set of Listeners.
     * @throws RemoteException The classic RMI exception to handle.
     */
    void onListenerAdd(Set<RemoteListener> remoteListeners) throws RemoteException;

}
