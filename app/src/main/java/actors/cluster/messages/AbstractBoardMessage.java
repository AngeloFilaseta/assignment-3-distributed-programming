package actors.cluster.messages;

import actors.cluster.util.LamportClock;
import actors.puzzleboard.Tile;

import java.util.List;

public abstract class AbstractBoardMessage extends PuzzleMessages {

    private final List<Tile> tiles;

    public AbstractBoardMessage(final LamportClock clock, final List<Tile> tiles) {
        super(clock);
        this.tiles = tiles;
    }

    /**
     * @return the tiles disposition of the message.
     */
    public List<Tile> getTiles() {
        return tiles;
    }

}
