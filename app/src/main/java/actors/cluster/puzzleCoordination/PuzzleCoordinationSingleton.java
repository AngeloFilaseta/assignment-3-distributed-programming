package actors.cluster.puzzleCoordination;

import actors.cluster.messages.updateBoard.UpdateBoardAck;
import actors.cluster.ModelSingleton;
import actors.cluster.messages.initBoard.InitializeBoardAck;
import actors.cluster.util.ClusterReferencesSingleton;
import actors.cluster.util.ClusterUtil;
import actors.cluster.util.CriticalSectionOperationsEnum;
import actors.cluster.util.LamportClock;
import actors.puzzleboard.Tile;
import akka.actor.Address;

import java.util.List;

/**
 * Global controller, see LockAcquisition and OperationExecution for finer granularity.
 */
public final class PuzzleCoordinationSingleton {

    private static PuzzleCoordinationSingleton instance = null;

    private final EnqueuedLockRequests enqueuedLockRequests;
    private final LockAcquisition lockAcquisition;
    private final OperationExecution operationExecution;
    private final Peers peers;
    private final MyCurrentLamportClockSingleton myLamportClock;

    private boolean initialized;

    private PuzzleCoordinationSingleton() {
        this.enqueuedLockRequests = EnqueuedLockRequests.createInstance();
        this.peers = Peers.createInstance();
        this.lockAcquisition = LockAcquisition.createInstance(enqueuedLockRequests, peers);
        this.operationExecution = OperationExecution.createInstance(this.lockAcquisition, this.peers);
        this.myLamportClock = MyCurrentLamportClockSingleton.getInstance();
        initialized = false;
    }

    public static PuzzleCoordinationSingleton getInstance() {
        if (instance == null) {
            instance = new PuzzleCoordinationSingleton();
        }
        return instance;
    }

    public void handlePeerLeaving(final Address address) {
        //remove eventual enqueued lock request
        enqueuedLockRequests.removeLockRequest(address);
        //remove from peer list (initialized or uninitialized not relevant, handled by peers structure)
        peers.removePeer(address);
        //in case of lock acquisition see if all ack are now possessed
        lockAcquisition.handlePeerLeaving(address);
        //in case of operation broadcasting see if all ack are now possessed
        operationExecution.handlePeerLeaving(address);
    }

    public void handlePeerJoining(final Address address) {
        if (ClusterUtil.isNewPeerMe(address)) {
            //initializing my lamport clock
            myLamportClock.setLamportClock(LamportClock.createInstance(address.toString()));
            //subsequent peers will be treated as uninitialized
            peers.startListeningForUninitializedPeers();
            //requesting for INITIALIZE operation
            requestOperation(CriticalSectionOperationsEnum.INITIALIZE);
        } else {
            //adding new peer (automatic initialized/uninitialized handling)
            peers.addPeer(address);
            //check if send lock request is needed
            lockAcquisition.handlePeerJoining(address);
        }
    }

    public void requestOperation(final CriticalSectionOperationsEnum operation) {
        lockAcquisition.startLockAcquisition(operation);
    }

    public void handleLockAckReceived(final Address address, final LamportClock ackClock) {
        lockAcquisition.handleLockAckReceived(address, ackClock);
    }

    public void handleLockRequestReceived(final Address address, final LamportClock reqClock) {
        lockAcquisition.handleLockRequestReceived(address, reqClock);
    }

    public void handleInitRequestReceived(final Address address, final LamportClock reqClock) {
        myLamportClock.updateMyCurrentClockIfNeeded(reqClock);
        myLamportClock.incrementMyLamportClock();
        System.out.println("Init request received from " + address.toString()
                + ", sending board status and promoting sender to initialized");
        //promoting uninitialized peer to initialized since it now has the board situation
        peers.promotePeerToInitialized(address);
        //sending InitializeBoardAck
        ClusterUtil.sendMessageTo(
                address,
                InitializeBoardAck.createInstance(
                        myLamportClock.getLamportClock(),
                        ModelSingleton.getInstance().getBoard().getListOfTiles()
                )
        );
    }

    public void handleInitAckReceived(final Address address, final List<Tile> tiles, final LamportClock reqClock) {
        myLamportClock.updateMyCurrentClockIfNeeded(reqClock);
        myLamportClock.incrementMyLamportClock();
        System.out.println("Init ack received from " + address.toString());
        if (!initialized) {
            //saving first board received
            initialized = true;
            ModelSingleton.updateInstance(ClusterReferencesSingleton.getInstance().getSelf(), tiles);
        }
        //handling operation ack
        operationExecution.handleOperationAckReceived(address);
    }

    public void handleUpdateRequestReceived(final Address address, final List<Tile> tiles, final LamportClock reqClock) {
        myLamportClock.updateMyCurrentClockIfNeeded(reqClock);
        myLamportClock.incrementMyLamportClock();
        System.out.println("Update request received from " + address.toString() + ", updating board");
        //updating my ModelSingleton using received board
        ModelSingleton.updateInstance(ClusterReferencesSingleton.getInstance().getSelf(), tiles);
        //sending UpdateBoardAck
        ClusterUtil.sendMessageTo(
                address,
                UpdateBoardAck.createInstance(myLamportClock.getLamportClock())
        );
    }

    public void handleUpdateAckReceived(final Address address, final LamportClock reqClock) {
        myLamportClock.updateMyCurrentClockIfNeeded(reqClock);
        myLamportClock.incrementMyLamportClock();
        System.out.println("Update ack received from " + address.toString());
        //handling operation ack
        operationExecution.handleOperationAckReceived(address);
    }

}
