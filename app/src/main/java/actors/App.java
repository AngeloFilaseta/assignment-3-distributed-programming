package actors;

import actors.cluster.PuzzleCluster;
import actors.cluster.util.ActorsNameEnum;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.log4j.BasicConfigurator;

import java.util.*;
import java.util.stream.IntStream;

public final class App {

    private static final int PORT = 25000;
    private static final int SEEDS_NUMBER = 10;

    private App() { }

    public static void main(final String[] args) {
        BasicConfigurator.configure();
        startup();
    }

    private static void startup() {
        int port = PORT;
        boolean found = false;
        List<String> seeds = new ArrayList<>();

        IntStream.rangeClosed(port, port + SEEDS_NUMBER).forEach(i -> seeds.add("akka://ClusterSystem@127.0.0.1:" + i));

        while (!found) {
            try {
                // Override the configuration of the port
                Map<String, Object> overrides = new HashMap<>();
                overrides.put("akka.remote.artery.canonical.port", port);
                overrides.put("akka.cluster.seed-nodes", seeds);

                Config config = ConfigFactory.parseMap(overrides).withFallback(ConfigFactory.load());

                // Create an Akka system
                ActorSystem system = ActorSystem.create(ActorsNameEnum.SYSTEM.toString(), config);
                system.actorOf(Props.create(PuzzleCluster.class), ActorsNameEnum.CLUSTER.toString());

                found = true;
            } catch (Exception e) {
                System.out.println("Port " + port + " already in use. Trying next...");
                port++;
            }
        }
    }

}
