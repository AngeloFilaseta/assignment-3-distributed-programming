package common.util;

public final class ResourceUtil {

    private ResourceUtil() {

    }

    public static String getImageFolderName() {
        return "src/main/resources/images/";
    }

    public static String getImageName() {
        return "bletchley-park-mansion.jpg";
    }
}
