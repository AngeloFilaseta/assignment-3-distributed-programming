package actors.cluster.messages.updateBoard;

import actors.cluster.messages.PuzzleMessages;
import actors.cluster.util.LamportClock;

public final class UpdateBoardAck extends PuzzleMessages {

    private UpdateBoardAck(final LamportClock clock) {
        super(clock);
    }

    public static UpdateBoardAck createInstance(final LamportClock clock) {
        return new UpdateBoardAck(clock);
    }
}
