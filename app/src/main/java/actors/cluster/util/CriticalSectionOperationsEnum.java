package actors.cluster.util;

public enum CriticalSectionOperationsEnum {

    NONE,
    INITIALIZE,
    UPDATE;

}
