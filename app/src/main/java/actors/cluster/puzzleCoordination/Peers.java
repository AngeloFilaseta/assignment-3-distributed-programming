package actors.cluster.puzzleCoordination;

import akka.actor.Address;

import java.util.HashSet;
import java.util.Set;

public final class Peers {

    private final Set<Address> initialized;
    private final Set<Address> uninitialized;
    private boolean listeningForUninitializedPeers;

    private Peers() {
        this.initialized = new HashSet<>();
        this.uninitialized = new HashSet<>();
        listeningForUninitializedPeers = false;
    }

    public static Peers createInstance() {
        return new Peers();
    }

    public void startListeningForUninitializedPeers() {
        listeningForUninitializedPeers = true;
    }

    public void addPeer(final Address address) {
        if (!uninitialized.contains(address) && !initialized.contains(address)) {
            if (listeningForUninitializedPeers) {
                System.out.println("Peer " + address.toString() + " has entered in the cluster as uninitialized peer");
                uninitialized.add(address);
            } else {
                System.out.println("Peer " + address.toString() + " has entered in the cluster as initialized peer");
                initialized.add(address);
            }
        }
    }

    public void promotePeerToInitialized(final Address address) {
        uninitialized.remove(address);
        initialized.add(address);
    }

    public Set<Address> getInitializedPeers() {
        return new HashSet<>(initialized);
    }

    public Set<Address> getAllPeers() {
        Set<Address> res = new HashSet<>(uninitialized);
        res.addAll(initialized);
        return res;
    }

    public void removePeer(final Address address) {
        System.out.println("Peer " + address.toString() + " leaving");
        uninitialized.remove(address);
        initialized.remove(address);
    }

    public boolean isLastPeerInCluster() {
        return getAllPeers().size() == 0;
    }

    public boolean isLastInitializedPeerInCluster() {
        return getInitializedPeers().size() == 0;
    }
}
