package rmi.model.local.singleton;

import rmi.model.local.LocalModel;
import rmi.model.local.LocalModelImpl;
import rmi.model.puzzleboard.Tile;

import java.util.Collections;
import java.util.List;

public final class LocalModelSingleton {

    private static LocalModel instance;

    private LocalModelSingleton() { }

    public static void createInstance() {
        createInstance(Collections.emptyList());
    }

    public static void createInstance(final List<Tile> newTileList) {
        if (instance == null) {
            instance = LocalModelImpl.createLocalModel(newTileList);
        } else {
            throw new IllegalCallerException("Cannot re-create this singleton instance");
        }
    }


    public static LocalModel getInstance() {
        if (instance == null) {
            throw new IllegalCallerException("Singleton instance called but was not created beforehand.");
        }
        return instance;
    }

}
