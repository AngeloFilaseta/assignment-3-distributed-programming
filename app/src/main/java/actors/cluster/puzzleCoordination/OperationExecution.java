package actors.cluster.puzzleCoordination;

import akka.actor.Address;

import java.util.HashSet;
import java.util.Set;

public final class OperationExecution {

    private final LockAcquisition lockAcquisition;
    private final Peers peers;
    private final Set<Address> operationAckReceived;

    private OperationExecution(final LockAcquisition lockAcquisition, final Peers peers) {
        this.lockAcquisition = lockAcquisition;
        this.peers = peers;
        operationAckReceived = new HashSet<>();
    }

    public static OperationExecution createInstance(final LockAcquisition lockAcquisition, final Peers peers) {
        return new OperationExecution(lockAcquisition, peers);
    }

    public void handlePeerLeaving(final Address address) {
        //not afflicted unless in critical section
        if (lockAcquisition.isLockAcquired()) {
            operationAckReceived.remove(address);
            if (peers.isLastPeerInCluster() || areAllAckFromInitializedPeersReceived()) {
                leaveCriticalSection();
            }
        }
    }

    public void handleOperationAckReceived(final Address address) {
        //not afflicted unless in critical section
        if (lockAcquisition.isLockAcquired()) {
            operationAckReceived.add(address);
            if (areAllAckFromInitializedPeersReceived()) {
                leaveCriticalSection();
            }
        }
    }

    private boolean areAllAckFromInitializedPeersReceived() {
        return operationAckReceived.size() == peers.getInitializedPeers().size();
    }

    private void leaveCriticalSection() {
        //clearing operation ack received
        operationAckReceived.clear();
        //releasing lock
        lockAcquisition.releaseLock();
    }
}
