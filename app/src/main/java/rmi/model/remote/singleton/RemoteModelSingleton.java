package rmi.model.remote.singleton;

import rmi.model.local.singleton.LocalModelSingleton;
import rmi.model.remote.RemoteModel;
import rmi.model.remote.name.RemoteName;
import rmi.model.remote.listener.RemoteListener;
import rmi.model.remote.listener.UpdateModelRemoteListener;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Singleton like class to wrap the Remote Container Object.
 */
public final class RemoteModelSingleton {

    private static RemoteModel instance;
    private static Registry registry;

    private static final int STOCK_SYSTEM_PORT = 0;

    private RemoteModelSingleton() { }

    /**
     * Create the singleton that wrap the RemoteTileContainer class.
     * Register a listener to get notified when its state changes.
     * @param remoteModel The Remote object to wrap in the singleton.
     * @throws RemoteException The classic RMI exception to handle.
     */
    public static void createInstance(final RemoteModel remoteModel) throws RemoteException {
        registry = LocateRegistry.getRegistry();
        rebindRemoteModel(remoteModel);
        registerListener(remoteModel, UpdateModelRemoteListener.createListener());
    }

    /**
     * Rebind the remote instance with the local instance.
     * Register each listener stored in local model (nodes listeners).
     * @param remoteModel The Remote object to wrap in the singleton.
     * @throws RemoteException The classic RMI exception to handle.
     */
    public static void rebindWithLocalInstance(final RemoteModel remoteModel) throws RemoteException {
        if (instance != null) {
            rebindRemoteModel(remoteModel);
            remoteModel.setListeners(LocalModelSingleton.getInstance().getRemoteListenersReferences());
        }
    }

    /**
     * @return the RemoteTileContainer in the singleton.
     */
    public static RemoteModel getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Instance not created beforehand.");
        }
        return instance;
    }


    /**
     * @param remoteModel the RemoteModel to bind in the registry.
     * @throws RemoteException The classic RMI exception to handle.
     */
    private static void rebindRemoteModel(final RemoteModel remoteModel) throws RemoteException {
        instance = remoteModel;
        UnicastRemoteObject.exportObject(instance, STOCK_SYSTEM_PORT);
        registry.rebind(RemoteName.MODEL.toString(), instance);
    }

    /**
     * Register a listener to get notified on object state change.
     * @param remoteTileContainer the RemoteTileContainer to attack the listener to.
     * @param remoteListener the remote listener to register.
     * @throws RemoteException The classic RMI exception to handle.
     */
    private static void registerListener(final RemoteModel remoteTileContainer, final RemoteListener remoteListener) throws RemoteException {
        UnicastRemoteObject.exportObject(remoteListener, STOCK_SYSTEM_PORT);
        remoteTileContainer.addListener(remoteListener);
    }


}
