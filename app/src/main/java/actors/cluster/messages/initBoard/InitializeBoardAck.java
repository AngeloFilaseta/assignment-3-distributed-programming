package actors.cluster.messages.initBoard;

import actors.cluster.messages.AbstractBoardMessage;
import actors.cluster.util.LamportClock;
import actors.puzzleboard.Tile;

import java.util.List;

public final class InitializeBoardAck extends AbstractBoardMessage {

    private InitializeBoardAck(final LamportClock clock, final List<Tile> tiles) {
        super(clock, tiles);
    }

    public static InitializeBoardAck createInstance(final LamportClock clock, final List<Tile> tiles) {
        return new InitializeBoardAck(clock, tiles);
    }

}
