package actors.cluster.util;

import actors.cluster.messages.PuzzleMessages;
import akka.actor.ActorSelection;
import akka.actor.Address;

public final class ClusterUtil {

    private ClusterUtil() {

    }

    public static Address getMyAddress() {
        return ClusterReferencesSingleton
                .getInstance()
                .getCluster()
                .remotePathOf(ClusterReferencesSingleton.getInstance().getSelf())
                .address();
    }

    public static Address getSenderAddress() {
        return ClusterReferencesSingleton
                .getInstance()
                .getCluster()
                .remotePathOf(ClusterReferencesSingleton.getInstance().getContext().sender())
                .address();
    }

    public static boolean isNewPeerMe(final Address address) {
        return getMyAddress().toString().equals(address.toString());
    }

    public static void sendMessageTo(final Address address, final PuzzleMessages message) {
        getActorSelectionFromAddress(address).tell(message, ClusterReferencesSingleton.getInstance().getSelf());
    }

    public static void broadcastTo(final Iterable<Address> addresses, final PuzzleMessages message) {
        addresses.forEach(a -> sendMessageTo(a, message));
    }

    public static ActorSelection getActorSelectionFromAddress(final Address address) {
        return ClusterReferencesSingleton
                .getInstance()
                .getContext()
                .actorSelection(address.toString() + "/user/" + ActorsNameEnum.CLUSTER);
    }

}
