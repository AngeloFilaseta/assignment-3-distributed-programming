package actors.cluster.puzzleCoordination;

import akka.actor.Address;

import java.util.HashSet;
import java.util.Set;

public final class EnqueuedLockRequests {

    private final Set<Address> lockRequests = new HashSet<>();

    private EnqueuedLockRequests() { }

    public static EnqueuedLockRequests createInstance() {
        return new EnqueuedLockRequests();
    }

    public void addLockRequest(final Address address) {
        this.lockRequests.add(address);
    }

    public void removeLockRequest(final Address address) {
        this.lockRequests.remove(address);
    }

    public Set<Address> clearLockRequests() {
        Set<Address> res = new HashSet<>(lockRequests);
        lockRequests.clear();
        return res;
    }
}
