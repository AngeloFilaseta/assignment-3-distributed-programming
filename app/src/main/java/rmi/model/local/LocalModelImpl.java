package rmi.model.local;

import rmi.model.puzzleboard.PuzzleBoard;
import rmi.model.puzzleboard.Tile;
import rmi.model.remote.listener.RemoteListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class LocalModelImpl implements LocalModel {

    private final Set<RemoteListener> remoteListenersReferences;
    private final PuzzleBoard board;

    private LocalModelImpl(final List<Tile> newTileList) {
        board = new PuzzleBoard("RMI Puzzle");
        remoteListenersReferences = new HashSet<>();
        if (newTileList.isEmpty()) {
            board.createTiles();
        } else {
            board.setListOfTiles(newTileList);
        }
        board.paintPuzzle(board.getPanel());
    }

    public static LocalModel createLocalModel(final List<Tile> newTileList) {
        return new LocalModelImpl(newTileList);
    }

    public PuzzleBoard getBoard() {
        return board;
    }

    public synchronized Set<RemoteListener> getRemoteListenersReferences() {
        return remoteListenersReferences;
    }

    public synchronized void setRemoteListenersReferences(final Set<RemoteListener> updatedRemoteListeners) {
        remoteListenersReferences.clear();
        remoteListenersReferences.addAll(updatedRemoteListeners);
    }

    public void setBoardVisible() {
        board.setVisible(true);
    }

}
