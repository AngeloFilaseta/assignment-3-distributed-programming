package actors.cluster.util;

import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.cluster.Cluster;

public final class ClusterReferencesSingleton {

    private static ClusterReferencesSingleton instance = null;

    private final Cluster cluster;
    private final ActorContext context;
    private final ActorRef self;

    private ClusterReferencesSingleton(final Cluster cluster, final ActorContext context, final ActorRef self) {
        this.cluster = cluster;
        this.context = context;
        this.self = self;
    }

    public static ClusterReferencesSingleton createInstance(final Cluster cluster, final ActorContext context, final ActorRef self) {
        if (instance == null) {
            instance = new ClusterReferencesSingleton(cluster, context, self);
        }
        return instance;
    }

    public static ClusterReferencesSingleton getInstance() {
        return instance;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public ActorContext getContext() {
        return context;
    }

    public ActorRef getSelf() {
        return self;
    }
}
