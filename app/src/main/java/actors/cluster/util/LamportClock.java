package actors.cluster.util;

import java.io.Serializable;

public final class LamportClock implements Serializable {

    private final String peerName;
    private final double value;

    private LamportClock(final String peerName, final double value) {
        this.peerName = peerName;
        this.value = value;
    }

    public static LamportClock createInstance(final String peerName, final double value) {
        return new LamportClock(peerName, value);
    }

    public static LamportClock createInstance(final String peerName) {
        return createInstance(peerName, 0);
    }

    public String getPeerName() {
        return peerName;
    }

    public double getValue() {
        return value;
    }

    public LamportClock increment(final String peerName) {
        return new LamportClock(peerName, value + 1);
    }

    public LamportClockCompareResult compareWith(final LamportClock clock) {
        if (value > clock.getValue()) {
            return LamportClockCompareResult.GREATER;
        } else if (value < clock.getValue()) {
            return LamportClockCompareResult.LESS;
        } else {
            return comparePeerNames(clock);
        }
    }

    private LamportClockCompareResult comparePeerNames(final LamportClock clock) {
        int strCompareResult = peerName.compareTo(clock.getPeerName());
        if (strCompareResult > 0) {
            return LamportClockCompareResult.GREATER;
        } else if (strCompareResult < 0) {
            return LamportClockCompareResult.LESS;
        } else {
            return LamportClockCompareResult.EQUAL;
        }
    }

    public enum LamportClockCompareResult {
        GREATER,
        EQUAL,
        LESS
    }
}
